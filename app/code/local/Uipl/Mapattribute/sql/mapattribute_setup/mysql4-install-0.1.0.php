<?php
$installer = $this;
$installer->startSetup();
$sql=<<<SQLTEXT
CREATE TABLE `map_attribute` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`model_year` INT( 11 ) NOT NULL ,
`model_year_label` VARCHAR( 255 ) NOT NULL,
`model_type` INT( 11 ) NOT NULL ,
`model_type_label` VARCHAR( 255 )NOT NULL,
`model` INT( 11 ) NOT NULL,
`model_label` VARCHAR( 255 )NOT NULL
)
SQLTEXT;

$installer->run($sql);
//demo 
//Mage::getModel('core/url_rewrite')->setId(null);
//demo 
$installer->endSetup();
	 