<?php
	
class Uipl_Mapattribute_Block_Adminhtml_Mapattr_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "id";
				$this->_blockGroup = "mapattribute";
				$this->_controller = "adminhtml_mapattr";
				$this->_updateButton("save", "label", Mage::helper("mapattribute")->__("Save Item"));
				$this->_updateButton("delete", "label", Mage::helper("mapattribute")->__("Delete Item"));

				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("mapattribute")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);



				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("mapattr_data") && Mage::registry("mapattr_data")->getId() ){

				    return Mage::helper("mapattribute")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("mapattr_data")->getId()));

				} 
				else{

				     return Mage::helper("mapattribute")->__("Add Item");

				}
		}
}