<?php

class Uipl_Mapattribute_Block_Adminhtml_Mapattr_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("mapattrGrid");
				$this->setDefaultSort("id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("mapattribute/mapattr")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("id", array(
				"header" => Mage::helper("mapattribute")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "id",
				));
                
				/*$this->addColumn("model_year_label", array(
				"header" => Mage::helper("mapattribute")->__("Model Year"),
				"index" => "model_year_label",
				));*/	
						$this->addColumn('model_year', array(
						'header' => Mage::helper('mapattribute')->__('Model Year'),
						'index' => 'model_year',
						'type' => 'options',
						'options'=>Uipl_Mapattribute_Block_Adminhtml_Mapattr_Grid::getOptionArray1(),				
						));
						
				/*$this->addColumn("model_type_label", array(
				"header" => Mage::helper("mapattribute")->__("Model Type"),
				"index" => "model_type_label",
				));*/
						$this->addColumn('model_type', array(
						'header' => Mage::helper('mapattribute')->__('Model Type'),
						'index' => 'model_type',
						'type' => 'options',
						'options'=>Uipl_Mapattribute_Block_Adminhtml_Mapattr_Grid::getOptionArray4(),				
						));
						
				/*$this->addColumn("model_label", array(
				"header" => Mage::helper("mapattribute")->__("Model"),
				"index" => "model_label",
				));*/
						$this->addColumn('model', array(
						'header' => Mage::helper('mapattribute')->__('Model'),
						'index' => 'model',
						'type' => 'options',
						'options'=>Uipl_Mapattribute_Block_Adminhtml_Mapattr_Grid::getOptionArray6(),				
						));
						
			//$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
			//$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('id');
			$this->getMassactionBlock()->setFormFieldName('ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_mapattr', array(
					 'label'=> Mage::helper('mapattribute')->__('Remove Mapattr'),
					 'url'  => $this->getUrl('*/adminhtml_mapattr/massRemove'),
					 'confirm' => Mage::helper('mapattribute')->__('Are you sure?')
				));
			return $this;
		}
			
		static public function getOptionArray1()
		{
            $data_array=array(); 
			
			
			$option_arr = array();
				$attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', 'model_year');
				foreach ($attribute->getSource()->getAllOptions(false) as $option) {
				  $option_arr[$option['value']] = $option['label'];
				}
				   return $option_arr;
			
           // return($data_array);
		}
		static public function getValueArray1()
		{
            $data_array=array();
			foreach(Uipl_Mapattribute_Block_Adminhtml_Mapattr_Grid::getOptionArray1() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		
		static public function getOptionArray4()
		{
            $data_array=array(); 
			//$data_array[0]='mini';
			//$data_array[1]='max';
			
			
			$option_arr = array();
				$attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', 'model_type');
				foreach ($attribute->getSource()->getAllOptions(false) as $option) {
				  $option_arr[$option['value']] = $option['label'];
				}
				   return $option_arr;
				
				
            //return($data_array);
		}
		static public function getValueArray4()
		{
            $data_array=array();
			foreach(Uipl_Mapattribute_Block_Adminhtml_Mapattr_Grid::getOptionArray4() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		
		static public function getOptionArray6()
		{
            $data_array=array(); 
			//$data_array[0]='onemodel';
			//$data_array[1]='twomodel';
			
			$option_arr = array();
				$attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', 'model');
				foreach ($attribute->getSource()->getAllOptions(false) as $option) {
				  $option_arr[$option['value']] = $option['label'];
				}
				   return $option_arr;
				
				
           // return($data_array);
		}
		static public function getValueArray6()
		{
            $data_array=array();
			foreach(Uipl_Mapattribute_Block_Adminhtml_Mapattr_Grid::getOptionArray6() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		

}