<?php
class Uipl_Mapattribute_Block_Adminhtml_Mapattr_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("mapattribute_form", array("legend"=>Mage::helper("mapattribute")->__("Item information")));

								
						 $fieldset->addField('model_year', 'select', array(
						'label'     => Mage::helper('mapattribute')->__('Model Year'),
						'values'   => Uipl_Mapattribute_Block_Adminhtml_Mapattr_Grid::getValueArray1(),
						'name' => 'model_year',					
						"class" => "required-entry",
						"required" => true,
						));				
						 $fieldset->addField('model_type', 'select', array(
						'label'     => Mage::helper('mapattribute')->__('Model Type'),
						'values'   => Uipl_Mapattribute_Block_Adminhtml_Mapattr_Grid::getValueArray4(),
						'name' => 'model_type',
						));				
						 $fieldset->addField('model', 'select', array(
						'label'     => Mage::helper('mapattribute')->__('Model'),
						'values'   => Uipl_Mapattribute_Block_Adminhtml_Mapattr_Grid::getValueArray6(),
						'name' => 'model',
						));

				if (Mage::getSingleton("adminhtml/session")->getMapattrData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getMapattrData());
					Mage::getSingleton("adminhtml/session")->setMapattrData(null);
				} 
				elseif(Mage::registry("mapattr_data")) {
				    $form->setValues(Mage::registry("mapattr_data")->getData());
				}
				return parent::_prepareForm();
		}
}
