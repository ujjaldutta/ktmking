<?php

class Uipl_Eventnews_Block_Adminhtml_Eventnews_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("eventnewsGrid");
				$this->setDefaultSort("id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("eventnews/eventnews")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("id", array(
				"header" => Mage::helper("eventnews")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "id",
				));
                
						/*$this->addColumn('category_id', array(
						'header' => Mage::helper('eventnews')->__('Category'),
						'index' => 'category_id',
						'type' => 'options',
						'options'=>Uipl_Eventnews_Block_Adminhtml_Eventnews_Grid::getOptionArray0(),				
						));*/
						
				$this->addColumn("title", array(
				"header" => Mage::helper("eventnews")->__("Title"),
				"index" => "title",
				));
					$this->addColumn('pubdate', array(
						'header'    => Mage::helper('eventnews')->__('Publish Date'),
						'index'     => 'pubdate',
						'type'      => 'datetime',
					));
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('id');
			$this->getMassactionBlock()->setFormFieldName('ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_eventnews', array(
					 'label'=> Mage::helper('eventnews')->__('Remove Eventnews'),
					 'url'  => $this->getUrl('*/adminhtml_eventnews/massRemove'),
					 'confirm' => Mage::helper('eventnews')->__('Are you sure?')
				));
			return $this;
		}
			
		static public function getOptionArray0()
		{
            $data_array=array(); 
			//$data_array[0]='abc';
			//$data_array[1]='xyz';
				$resource = Mage::getSingleton('core/resource');
				$readConnection = $resource->getConnection('core_read');
				$writeConnection = $resource->getConnection('core_write');
				
				$query = 'SELECT * FROM news_category where status=1';
				$results = $readConnection->fetchAll($query);
				
			$option_arr = array();
				$attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', 'model_type');
				foreach ($results as $option) {
				  $option_arr[$option['id']] = $option['title'];
				}
				
				
            return($option_arr);
		}
		static public function getValueArray0()
		{
            $data_array=array();
			foreach(Uipl_Eventnews_Block_Adminhtml_Eventnews_Grid::getOptionArray0() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		

}