<?php


class Uipl_Mapattribute_Block_Adminhtml_Mapattr extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_mapattr";
	$this->_blockGroup = "mapattribute";
	$this->_headerText = Mage::helper("mapattribute")->__("Mapattr Manager");
	$this->_addButtonLabel = Mage::helper("mapattribute")->__("Add New Item");
	parent::__construct();
	
	}

}