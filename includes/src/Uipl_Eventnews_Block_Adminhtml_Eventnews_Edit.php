<?php
	
class Uipl_Eventnews_Block_Adminhtml_Eventnews_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "id";
				$this->_blockGroup = "eventnews";
				$this->_controller = "adminhtml_eventnews";
				$this->_updateButton("save", "label", Mage::helper("eventnews")->__("Save Item"));
				$this->_updateButton("delete", "label", Mage::helper("eventnews")->__("Delete Item"));

				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("eventnews")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);



				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("eventnews_data") && Mage::registry("eventnews_data")->getId() ){

				    return Mage::helper("eventnews")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("eventnews_data")->getId()));

				} 
				else{

				     return Mage::helper("eventnews")->__("Add Item");

				}
		}
}