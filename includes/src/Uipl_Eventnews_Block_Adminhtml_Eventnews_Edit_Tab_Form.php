<?php
class Uipl_Eventnews_Block_Adminhtml_Eventnews_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		
		
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("eventnews_form", array("legend"=>Mage::helper("eventnews")->__("Item information")));

								
						 /*$fieldset->addField('category_id', 'select', array(
						'label'     => Mage::helper('eventnews')->__('Category'),
						'values'   => Uipl_Eventnews_Block_Adminhtml_Eventnews_Grid::getValueArray0(),
						'name' => 'category_id',
						));*/
						$fieldset->addField("title", "text", array(
						"label" => Mage::helper("eventnews")->__("Title"),					
						"class" => "required-entry",
						"required" => true,
						"name" => "title",
						));
					
						$fieldset->addField("details", "editor", array(
						"label" => Mage::helper("eventnews")->__("Details"),					
						"class" => "required-entry",
						"required" => true,
						"name" => "details",
						'style'     => 'height:36em;width:60em;',
						'config'    => Mage::getSingleton('cms/wysiwyg_config')->getConfig()
						));
						
						 
					
						$dateFormatIso = Mage::app()->getLocale()->getDateTimeFormat(
							Mage_Core_Model_Locale::FORMAT_TYPE_SHORT
						);

						$fieldset->addField('pubdate', 'date', array(
						'label'        => Mage::helper('eventnews')->__('Publish Date'),
						'name'         => 'pubdate',					
						"class" => "required-entry",
						"required" => true,
						'time' => true,
						'image'        => $this->getSkinUrl('images/grid-cal.gif'),
						'format'       => $dateFormatIso
						));				
						$fieldset->addField('image', 'image', array(
						'label' => Mage::helper('eventnews')->__('Image'),
						'name' => 'image',
						'note' => '(*.jpg, *.png, *.gif)',
						));

				if (Mage::getSingleton("adminhtml/session")->getEventnewsData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getEventnewsData());
					Mage::getSingleton("adminhtml/session")->setEventnewsData(null);
				} 
				elseif(Mage::registry("eventnews_data")) {
				    $form->setValues(Mage::registry("eventnews_data")->getData());
				}
				return parent::_prepareForm();
		}
}
