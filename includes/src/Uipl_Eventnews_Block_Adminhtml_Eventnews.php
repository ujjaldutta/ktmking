<?php


class Uipl_Eventnews_Block_Adminhtml_Eventnews extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_eventnews";
	$this->_blockGroup = "eventnews";
	$this->_headerText = Mage::helper("eventnews")->__("Eventnews Manager");
	$this->_addButtonLabel = Mage::helper("eventnews")->__("Add New Item");
	parent::__construct();
	
	}

}