<?php
class Uipl_Mapattribute_Block_Adminhtml_Mapattr_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("mapattr_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("mapattribute")->__("Item Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("mapattribute")->__("Item Information"),
				"title" => Mage::helper("mapattribute")->__("Item Information"),
				"content" => $this->getLayout()->createBlock("mapattribute/adminhtml_mapattr_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
