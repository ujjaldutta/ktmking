<?php
class Uipl_Mapattribute_IndexController extends Mage_Core_Controller_Front_Action{
    public function IndexAction() {
      
	  $this->loadLayout();   
	  $this->getLayout()->getBlock("head")->setTitle($this->__("Titlename"));
	        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
      $breadcrumbs->addCrumb("home", array(
                "label" => $this->__("Home Page"),
                "title" => $this->__("Home Page"),
                "link"  => Mage::getBaseUrl()
		   ));

      $breadcrumbs->addCrumb("titlename", array(
                "label" => $this->__("Titlename"),
                "title" => $this->__("Titlename")
		   ));

      $this->renderLayout(); 
	  
    }
    
 //ujjal : ajax home model type dropdown respection of year   
    public function getModeltypeAction(){
	
	try{
	    $filtercol= Mage::getModel('mapattribute/mapattr')->getCollection()
	       ->addFieldToFilter('model_year', array('eq' => $this->getRequest()->getPost("year") ));
	    $filtercol->getSelect()->group(array('main_table.model_type'));
      
		    $option_arr = array();
		    $attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', 'model_type');
			foreach ($attribute->getSource()->getAllOptions(false) as $option) {
				  $option_arr[$option['value']] = $option['label'];
				}
	    $option='<option value="">Select Model Type</option>';
	
	    foreach($filtercol as $row){
	     
	      $type= $row->getModelType();
		     if($option_arr[$type]!=''){
		     $option .='<option value="'.$type.'">'.$option_arr[$type].'</option>';
		    }
	    }
	    
	} catch (Exception $e) { 
	
	
       }
      
       echo $option;
       exit;
    }
    
    
    
    
    
    
    //ujjal : ajax home model type dropdown respection of year   
    public function getModelAction(){
	
	try{
	    $filtercol= Mage::getModel('mapattribute/mapattr')->getCollection()
	        ->addFieldToFilter('model_year', array('eq' => $this->getRequest()->getPost("year") ))
	       ->addFieldToFilter('model_type', array('eq' => $this->getRequest()->getPost("modeltype") ));
	    $filtercol->getSelect()->group(array('main_table.model'));
      
		    $option_arr = array();
		    $attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', 'model');
			foreach ($attribute->getSource()->getAllOptions(false) as $option) {
				  $option_arr[$option['value']] = $option['label'];
				}
	    $option='<option value="">Select Model</option>';
	
	    foreach($filtercol as $row){
	     
	      $type= $row->getModel();
		     if($option_arr[$type]!=''){
		     $option .='<option value="'.$type.'">'.$option_arr[$type].'</option>';
		    }
	    }
	    
	} catch (Exception $e) { 
	
	
       }
      
       echo $option;
       exit;
    }
    
    public function getUrlAction(){
	$yeartext=$this->getRequest()->getPost("yeartext");
	$yeartext=str_replace(" ","-",$yeartext);
	$modeltypetext=strtolower($this->getRequest()->getPost("modeltypetext"));
	$modeltypetext=str_replace(" ","-",$modeltypetext);
	$modeltext=strtolower($this->getRequest()->getPost("modeltext"));
	$modeltext=str_replace(" ","-",$modeltext);
	
	$url=Mage::getUrl();
	$url=$url.'spare-parts-kits/filter/model/'.$modeltext.'/model_type/'.$modeltypetext.'/model_year/'.$yeartext.'.html';
	echo $url;exit;
    }
    
}